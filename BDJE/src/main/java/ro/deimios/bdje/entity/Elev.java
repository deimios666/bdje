package ro.deimios.bdje.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author deimios
 */
@Entity
@Table(name = "elev")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Elev.findAll", query = "SELECT e FROM Elev e"),
    @NamedQuery(name = "Elev.findByCnp", query = "SELECT e FROM Elev e WHERE e.cnp = :cnp"),
    @NamedQuery(name = "Elev.findByPJ", query = "SELECT e FROM Elev e WHERE e.unitate.superior.sirues = :sirues"),
    @NamedQuery(name = "Elev.findByNume", query = "SELECT e FROM Elev e WHERE e.nume = :nume"),
    @NamedQuery(name = "Elev.findByIni", query = "SELECT e FROM Elev e WHERE e.ini = :ini"),
    @NamedQuery(name = "Elev.findByPrenume", query = "SELECT e FROM Elev e WHERE e.prenume = :prenume"),
    @NamedQuery(name = "Elev.findByNumeClasa", query = "SELECT e FROM Elev e WHERE e.numeClasa = :numeClasa")})
public class Elev implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "cnp")
    private Long cnp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "nume")
    private String nume;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 16)
    @Column(name = "ini")
    private String ini;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "prenume")
    private String prenume;
    @Size(max = 45)
    @Column(name = "nume_clasa")
    private String numeClasa;
    @JoinColumn(name = "alternativa", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Alternativa alternativa;
    @JoinColumn(name = "mediul", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Mediul mediul;
    @JoinColumn(name = "sectia", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Sectia sectia;
    @JoinColumn(name = "forma", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Forma forma;
    @JoinColumn(name = "clasa", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Clasa clasa;
    @JoinColumn(name = "unitate", referencedColumnName = "sirues")
    @ManyToOne(optional = false)
    private Unitate unitate;

    public Elev() {
    }

    public Elev(Long cnp) {
        this.cnp = cnp;
    }

    public Elev(Long cnp, String nume, String ini, String prenume) {
        this.cnp = cnp;
        this.nume = nume;
        this.ini = ini;
        this.prenume = prenume;
    }

    public Long getCnp() {
        return cnp;
    }

    public void setCnp(Long cnp) {
        this.cnp = cnp;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getIni() {
        return ini;
    }

    public void setIni(String ini) {
        this.ini = ini;
    }

    public String getPrenume() {
        return prenume;
    }

    public void setPrenume(String prenume) {
        this.prenume = prenume;
    }

    public String getNumeClasa() {
        return numeClasa;
    }

    public void setNumeClasa(String numeClasa) {
        this.numeClasa = numeClasa;
    }

    public Alternativa getAlternativa() {
        return alternativa;
    }

    public void setAlternativa(Alternativa alternativa) {
        this.alternativa = alternativa;
    }

    public Mediul getMediul() {
        return mediul;
    }

    public void setMediul(Mediul mediul) {
        this.mediul = mediul;
    }

    public Sectia getSectia() {
        return sectia;
    }

    public void setSectia(Sectia sectia) {
        this.sectia = sectia;
    }

    public Forma getForma() {
        return forma;
    }

    public void setForma(Forma forma) {
        this.forma = forma;
    }

    public Clasa getClasa() {
        return clasa;
    }

    public void setClasa(Clasa clasa) {
        this.clasa = clasa;
    }

    public Unitate getUnitate() {
        return unitate;
    }

    public void setUnitate(Unitate unitate) {
        this.unitate = unitate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cnp != null ? cnp.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Elev)) {
            return false;
        }
        Elev other = (Elev) object;
        if ((this.cnp == null && other.cnp != null) || (this.cnp != null && !this.cnp.equals(other.cnp))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ro.deimios.bdje.entity.Elev[ cnp=" + cnp + " ]";
    }
    
}
