package ro.deimios.bdje.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author deimios
 */
@Entity
@Table(name = "alternativa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Alternativa.findAll", query = "SELECT a FROM Alternativa a"),
    @NamedQuery(name = "Alternativa.findById", query = "SELECT a FROM Alternativa a WHERE a.id = :id"),
    @NamedQuery(name = "Alternativa.findByNume", query = "SELECT a FROM Alternativa a WHERE a.nume = :nume")})
public class Alternativa implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 145)
    @Column(name = "nume")
    private String nume;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "alternativa")
    private List<Elev> elevList;

    public Alternativa() {
    }

    public Alternativa(Integer id) {
        this.id = id;
    }

    public Alternativa(Integer id, String nume) {
        this.id = id;
        this.nume = nume;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    @XmlTransient
    public List<Elev> getElevList() {
        return elevList;
    }

    public void setElevList(List<Elev> elevList) {
        this.elevList = elevList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Alternativa)) {
            return false;
        }
        Alternativa other = (Alternativa) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ro.deimios.bdje.entity.Alternativa[ id=" + id + " ]";
    }
    
}
