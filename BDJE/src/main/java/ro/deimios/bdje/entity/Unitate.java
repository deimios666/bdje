/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.bdje.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author deimios
 */
@Entity
@Table(name = "unitate")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Unitate.findAll", query = "SELECT u FROM Unitate u order by u.denumire"),
    @NamedQuery(name = "Unitate.findBySirues", query = "SELECT u FROM Unitate u WHERE u.sirues = :sirues"),
    @NamedQuery(name = "Unitate.findByDenumire", query = "SELECT u FROM Unitate u WHERE u.denumire = :denumire"),
    @NamedQuery(name = "Unitate.findBySuperior", query = "SELECT u FROM Unitate u WHERE u.superior.sirues = :sirues order by u.denumire"),
    @NamedQuery(name = "Unitate.findByTipunitate", query = "SELECT u FROM Unitate u WHERE u.tipunitate = :tipunitate")})
public class Unitate implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "sirues")
    private Integer sirues;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "denumire")
    private String denumire;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tipunitate")
    private int tipunitate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "unitate")
    private List<Elev> elevList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "unitate")
    private List<User> userList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "superior")
    private List<Unitate> unitateList;
    @JoinColumn(name = "superior", referencedColumnName = "sirues")
    @ManyToOne(optional = false)
    private Unitate superior;

    public Unitate() {
    }

    public Unitate(Integer sirues) {
        this.sirues = sirues;
    }

    public Unitate(Integer sirues, String denumire, int tipunitate) {
        this.sirues = sirues;
        this.denumire = denumire;
        this.tipunitate = tipunitate;
    }

    public Integer getSirues() {
        return sirues;
    }

    public void setSirues(Integer sirues) {
        this.sirues = sirues;
    }

    public String getDenumire() {
        return denumire;
    }

    public void setDenumire(String denumire) {
        this.denumire = denumire;
    }

    public int getTipunitate() {
        return tipunitate;
    }

    public void setTipunitate(int tipunitate) {
        this.tipunitate = tipunitate;
    }

    @XmlTransient
    public List<Elev> getElevList() {
        return elevList;
    }

    public void setElevList(List<Elev> elevList) {
        this.elevList = elevList;
    }

    @XmlTransient
    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    @XmlTransient
    public List<Unitate> getUnitateList() {
        return unitateList;
    }

    public void setUnitateList(List<Unitate> unitateList) {
        this.unitateList = unitateList;
    }

    public Unitate getSuperior() {
        return superior;
    }

    public void setSuperior(Unitate superior) {
        this.superior = superior;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sirues != null ? sirues.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Unitate)) {
            return false;
        }
        Unitate other = (Unitate) object;
        if ((this.sirues == null && other.sirues != null) || (this.sirues != null && !this.sirues.equals(other.sirues))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ro.deimios.bdje.entity.Unitate[ sirues=" + sirues + " ]";
    }
    
}
