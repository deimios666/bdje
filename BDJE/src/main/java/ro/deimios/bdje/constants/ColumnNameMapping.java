/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.bdje.constants;

import java.util.HashMap;

/**
 *
 * @author deimios
 */
public class ColumnNameMapping {

    private static final HashMap<String, String> nameMap;

    static {
        nameMap = new HashMap<>();
        nameMap.put("Total", "Total");
        nameMap.put("unitate.superior.denumire", "Unitate");
        nameMap.put("unitate.denumire", "Structură");
        nameMap.put("clasa.nume", "Grupa/Clasa");
        nameMap.put("alternativa.nume", "Alternativa");
        nameMap.put("sectia.nume", "Secția");
        nameMap.put("forma.nume", "Forma");
        nameMap.put("mediul.nume", "Mediu");
    }

    public static String getColName(String name) {
        String value = nameMap.get(name);
        if (value == null) {
            return name;
        } else {
            return value;
        }
    }
}
