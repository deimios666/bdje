package ro.deimios.bdje.beans;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import ro.deimios.bdje.entity.User;
import ro.deimios.bdje.facade.UserFacadeLocal;
import ro.deimios.bdje.facade.helper.CryptHelper;

/**
 *
 * @author deimios
 */
@ManagedBean
@SessionScoped
public class UserBean implements Serializable{

    @EJB
    private UserFacadeLocal userFacade;
    boolean isLoggedIn = false;
    boolean ISJ = false;
    String username;
    String password;
    User user;

    /**
     * Creates a new instance of UserBean
     */
    public UserBean() {
    }

    public void doLogin() {
        User dbUser = userFacade.findByLogin(username, password);

        if (dbUser != null) {
            user = dbUser;
            ISJ = user.getTipUser() == 1;
            password = null;
            isLoggedIn = true;
        } else {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Eroare Autentificare", "Nume utilizator sau parolă incorectă");
            FacesContext.getCurrentInstance().addMessage("loginMessages", msg);
        }

    }
    
    public void doLogout() {
        isLoggedIn = false;
        user = null;
        ISJ = false;
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        request.getSession(true).invalidate();
    }

    public boolean isIsLoggedIn() {
        return isLoggedIn;
    }

    public boolean isISJ() {
        return ISJ;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = CryptHelper.sha512(password);
    }
}
