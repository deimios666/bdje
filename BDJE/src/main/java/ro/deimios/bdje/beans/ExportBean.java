package ro.deimios.bdje.beans;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Name;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import ro.deimios.bdje.entity.Elev;
import ro.deimios.bdje.entity.Unitate;
import ro.deimios.bdje.facade.ElevFacadeLocal;
import ro.deimios.bdje.facade.UnitateFacadeLocal;
import ro.deimios.bdje.facade.helper.BeanHelper;

/**
 *
 * @author deimios
 */
@ManagedBean
@RequestScoped
public class ExportBean {

    public static int EXPORT_TYPE_EMPTY = 0;
    public static int EXPORT_TYPE_FILLED = 1;
    public static int EXPORT_TYPE_COMPLETE = 2;
    @EJB
    private UnitateFacadeLocal unitateFacade;
    private static final Logger LOG = Logger.getLogger(ExportBean.class.getName());
    @EJB
    private ElevFacadeLocal elevFacade;

    /**
     * Creates a new instance of ExportBean
     */
    public ExportBean() {
    }

    public void exportMacheta(int export_type) {
        InputStream templateInputStream;
        OutputStream output;
        Workbook template;
        Sheet mainSheet, nomenSheet;
        List<Elev> elevList = new ArrayList<>();
        List<Unitate> structuraList;
        CellStyle cnpStyle, textStyle;
        int headerOffset = 1;
        int maxRows = 0;

        if (export_type == EXPORT_TYPE_FILLED) {
            maxRows = 11;
        } else if (export_type == EXPORT_TYPE_COMPLETE) {
            maxRows = 15;
        }

        UserBean userBean = (UserBean) BeanHelper.getBean("userBean");
        int sirues = userBean.user.getUnitate().getSirues();
        FacesContext fc = FacesContext.getCurrentInstance();
        ExternalContext ec = fc.getExternalContext();
        ec.responseReset(); // Some JSF component library or some Filter might have set some headers in the buffer beforehand. We want to get rid of them, else it may collide.
        ec.setResponseContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"); // Check http://www.iana.org/assignments/media-types for all types. Use if necessary ExternalContext#getMimeType() for auto-detection based on filename.
        ec.setResponseHeader("Content-Disposition", "attachment; filename=\"" + "macheta.xlsx" + "\""); // The Save As popup magic is done here. You can give it any file name you want, this only won't work in MSIE, it will use current request URL as file name instead.
        try {
            templateInputStream = ec.getResourceAsStream("/resources/files/macheta.xlsx");
            template = WorkbookFactory.create(templateInputStream);



            mainSheet = template.getSheetAt(0);
            nomenSheet = template.getSheetAt(1);

            DataFormat format = template.createDataFormat();
            cnpStyle = template.createCellStyle();
            cnpStyle.setDataFormat(format.getFormat("#0"));



            if (userBean.isISJ()) {
                if (export_type != EXPORT_TYPE_EMPTY) {
                    elevList = elevFacade.findAll();
                }
                structuraList = unitateFacade.findAllOrdered();
                //LOG.info("User is ISJ");
            } else {
                if (export_type != EXPORT_TYPE_EMPTY) {
                    elevList = elevFacade.findByPJ(sirues);
                }
                structuraList = unitateFacade.findBySuperior(sirues);
                //LOG.info("User isn't ISJ");
            }

            int i;

            for (i = 0; i < structuraList.size(); i++) {
                Unitate unitate = structuraList.get(i);
                Row row = nomenSheet.getRow(i + 1);
                if (row == null) {
                    row = nomenSheet.createRow(i + 1);
                }
                row.createCell(0).setCellValue(unitate.getDenumire());
            }

            String unitateReference = "nomenclator!$A$2:$A$" + (i + 1);
            int unitateIndex = template.getNameIndex("unitatea");
            Name namedCell = template.getNameAt(unitateIndex);
            namedCell.setRefersToFormula(unitateReference);

            if (export_type != EXPORT_TYPE_EMPTY) {
                int elevListSize = elevList.size();

                //LOG.log(Level.INFO, "Elevlist size: {0}", elevListSize);

                for (i = 0; i < elevListSize; i++) {
                    Elev elev = elevList.get(i);
                    Row currentRow = mainSheet.getRow(i + headerOffset);
                    if (currentRow == null) {
                        currentRow = mainSheet.createRow(i + headerOffset);
                    }
                    for (int j = 0; j < maxRows; j++) {
                        Cell newCell = currentRow.createCell(j);
                    }
                    currentRow.createCell(0, Cell.CELL_TYPE_NUMERIC);
                    currentRow.getCell(0).setCellStyle(cnpStyle);
                    currentRow.getCell(0).setCellValue(elev.getCnp());
                    currentRow.getCell(1).setCellValue(elev.getNume());
                    currentRow.getCell(2).setCellValue(elev.getIni());
                    currentRow.getCell(3).setCellValue(elev.getPrenume());
                    currentRow.getCell(4).setCellValue(elev.getUnitate().getDenumire());
                    currentRow.getCell(5).setCellValue(elev.getClasa().getNume());
                    currentRow.getCell(6).setCellValue(elev.getNumeClasa());
                    currentRow.getCell(7).setCellValue(elev.getAlternativa().getNume());
                    currentRow.getCell(8).setCellValue(elev.getSectia().getNume());
                    currentRow.getCell(9).setCellValue(elev.getForma().getNume());
                    currentRow.getCell(10).setCellValue(elev.getMediul().getNume());
                    if (export_type == EXPORT_TYPE_COMPLETE) {
                        currentRow.getCell(11).setCellValue(elev.getUnitate().getSuperior().getSirues());
                        currentRow.getCell(12).setCellValue(elev.getUnitate().getSuperior().getDenumire());
                        currentRow.getCell(13).setCellValue(elev.getUnitate().getSirues());
                        currentRow.getCell(14).setCellValue(elev.getUnitate().getDenumire());
                    }
                }
            }
            templateInputStream.close();
            output = ec.getResponseOutputStream();
            template.write(output);
            output.flush();
            output.close();
            fc.responseComplete(); // Important! Otherwise JSF will attempt to render the response which obviously will fail since it's already written with a file and closed.

        } catch (IOException | InvalidFormatException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }


    }

    public void exportMachetaEmpty() {
        exportMacheta(EXPORT_TYPE_EMPTY);
    }

    public void exportMachetaFilled() {
        exportMacheta(EXPORT_TYPE_FILLED);
    }

    public void exportMachetaComplete() {
        exportMacheta(EXPORT_TYPE_COMPLETE);
    }
}
