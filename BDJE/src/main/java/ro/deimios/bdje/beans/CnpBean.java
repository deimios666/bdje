/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.bdje.beans;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import ro.deimios.bdje.facade.helper.CNPHelper;

/**
 *
 * @author deimios
 */
@ManagedBean
@RequestScoped
public class CnpBean implements Serializable{

    public static final Date Y2K;
    public static SimpleDateFormat sdf;

    static {
        Y2K = new Date(100, 0, 1);
    }

    static {
        sdf = new SimpleDateFormat("yyMMdd");
    }
    String cnp, sex;
    Date data_nasterii;

    /**
     * Creates a new instance of CnpBean
     */
    public CnpBean() {
    }

    public void generateCNP() {
        cnp = "Eroare!";
        String tmpCnp = "0";
        if (data_nasterii != null) {
            if (data_nasterii.before(Y2K)) {
                if (sex.equalsIgnoreCase("m")) {
                    tmpCnp = "1";
                } else {
                    tmpCnp = "2";
                }
            } else {
                if (sex.equalsIgnoreCase("m")) {
                    tmpCnp = "5";
                } else {
                    tmpCnp = "6";
                }
            }
        } else {
            return;
        }
        tmpCnp += sdf.format(data_nasterii);
        tmpCnp += "14";
        int rng = 100 + (int) (Math.random() * 899);
        tmpCnp += rng + "";
        tmpCnp += CNPHelper.getCheckSum(tmpCnp) + "";
        cnp=tmpCnp;
    }

    public String getCnp() {
        return cnp;
    }

    public void setCnp(String cnp) {
        this.cnp = cnp;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Date getData_nasterii() {
        return data_nasterii;
    }

    public void setData_nasterii(Date data_nasterii) {
        this.data_nasterii = data_nasterii;
    }
}
