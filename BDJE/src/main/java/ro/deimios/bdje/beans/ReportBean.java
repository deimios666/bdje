package ro.deimios.bdje.beans;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import ro.deimios.bdje.constants.ColumnNameMapping;
import ro.deimios.bdje.facade.ElevFacadeLocal;
import ro.deimios.bdje.facade.helper.BeanHelper;

/**
 *
 * @author deimios
 */
@ManagedBean
@ViewScoped
public class ReportBean implements Serializable {
    
    @EJB
    private ElevFacadeLocal elevFacade;
    private boolean nr_crt = false, total_row = true, total_col = true;
    private List<SelectItem> group_values, col_values;
    String group_row1, group_row2, group_row3, group_col1;
    private static final Logger LOG = Logger.getLogger(ReportBean.class.getName());
    
    @PostConstruct
    public void loadData() {
        group_values = new ArrayList<>();
        group_values.add(new SelectItem("", "Fără defalcare"));
        group_values.add(new SelectItem("unitate.superior.denumire", "Unitate"));
        group_values.add(new SelectItem("unitate.denumire", "Structură"));
        group_values.add(new SelectItem("clasa.nume", "Grupa/Clasa"));
        group_values.add(new SelectItem("alternativa.nume", "Alternativa"));
        group_values.add(new SelectItem("sectia.nume", "Secția"));
        group_values.add(new SelectItem("forma.nume", "Forma"));
        group_values.add(new SelectItem("mediul.nume", "Mediu"));
        col_values = new ArrayList<>();
        col_values.add(new SelectItem("", "Fără defalcare"));
        col_values.add(new SelectItem("Clasa", "Grupa/Clasa"));
        col_values.add(new SelectItem("Alternativa", "Alternativa"));
        col_values.add(new SelectItem("Sectia", "Secția"));
        col_values.add(new SelectItem("Forma", "Forma"));
        col_values.add(new SelectItem("Mediul", "Mediu"));
    }

    /**
     * Creates a new instance of ReportBean
     */
    public ReportBean() {
    }
    
    public void generateReport() {
        List<String> criteria = new ArrayList<>();
        if (group_row1 != null && !group_row1.isEmpty()) {
            criteria.add(group_row1);
        }
        if (group_row2 != null && !group_row2.isEmpty()) {
            criteria.add(group_row2);
        }
        if (group_row3 != null && !group_row3.isEmpty()) {
            criteria.add(group_row3);
        }
        
        
        
        InputStream templateInputStream;
        OutputStream output;
        Workbook template;
        Sheet mainSheet;
        CellStyle headerStyle, textStyle;
        int headerOffset = 1;
        UserBean userBean = (UserBean) BeanHelper.getBean("userBean");
        int sirues = userBean.user.getUnitate().getSirues();
        List<Object[]> values;
        
        if (userBean.isISJ()) {
            values = elevFacade.getReport(group_col1, criteria);
        } else {
            values = elevFacade.getReport(group_col1, criteria, sirues);
        }
        
        FacesContext fc = FacesContext.getCurrentInstance();
        ExternalContext ec = fc.getExternalContext();
        ec.responseReset(); // Some JSF component library or some Filter might have set some headers in the buffer beforehand. We want to get rid of them, else it may collide.
        ec.setResponseContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"); // Check http://www.iana.org/assignments/media-types for all types. Use if necessary ExternalContext#getMimeType() for auto-detection based on filename.
        ec.setResponseHeader("Content-Disposition", "attachment; filename=\"" + "raport.xlsx" + "\""); // The Save As popup magic is done here. You can give it any file name you want, this only won't work in MSIE, it will use current request URL as file name instead.
        try {
            
            templateInputStream = ec.getResourceAsStream("/resources/files/report.xlsx");
            template = WorkbookFactory.create(templateInputStream);
            mainSheet = template.getSheetAt(0);
            headerStyle = mainSheet.getRow(0).getCell(0).getCellStyle();
            textStyle = mainSheet.getRow(1).getCell(0).getCellStyle();

            //write header
            int curCell = 0;
            Row headerRow = mainSheet.getRow(0);
            if (nr_crt) {
                headerRow.getCell(curCell).setCellValue("Nr. Crt.");
                curCell++;
            }
            Object[] headerValues = values.get(0);
            for (int headerWalker = 0; headerWalker < headerValues.length; headerWalker++) {
                Cell cell = headerRow.createCell(curCell);
                cell.setCellStyle(headerStyle);
                cell.setCellValue(ColumnNameMapping.getColName((String) headerValues[headerWalker]));
                curCell++;
            }

            //skip colnames, start from 1

            for (int rowWalker = 1; rowWalker < values.size(); rowWalker++) {
                curCell = 0;
                Object[] curDataRow;
                if (values.get(rowWalker) instanceof Object[]) {
                    curDataRow = values.get(rowWalker);
                } else {
                    curDataRow = new Object[1];
                    curDataRow[0] = values.get(rowWalker);
                }
                Row row = mainSheet.createRow(rowWalker);
                if (nr_crt) {
                    Cell crtCell = row.createCell(0);
                    crtCell.setCellStyle(textStyle);
                    crtCell.setCellValue(rowWalker);
                    curCell++;
                }
                
                for (int dataColWalker = 0; dataColWalker < curDataRow.length; dataColWalker++) {
                    Cell dataCell = row.createCell(curCell);
                    dataCell.setCellStyle(textStyle);
                    //LOG.info("[R" + rowWalker + ",C" + curCell + "]=" + curDataRow[dataColWalker].toString());
                    //dataCell.setCellValue((String) curDataRow[dataRowWalker]);
                    if (curDataRow[dataColWalker] instanceof String) {
                        dataCell.setCellValue((String) curDataRow[dataColWalker]);
                    } else if (curDataRow[dataColWalker] instanceof BigDecimal) {
                        dataCell.setCellValue(((BigDecimal) curDataRow[dataColWalker]).longValue());
                    } else if (curDataRow[dataColWalker] instanceof Long) {
                        dataCell.setCellValue((Long) curDataRow[dataColWalker]);
                    } else {
                        dataCell.setCellValue("ERROR");
                    }
                    curCell++;
                }
                
            }
            
            templateInputStream.close();
            output = ec.getResponseOutputStream();
            template.write(output);
            output.flush();
            output.close();
        } catch (IOException ioex) {
            LOG.log(Level.SEVERE, "IO Exception while saving report: {0}", ioex);
            
        } catch (InvalidFormatException ex) {
            LOG.log(Level.SEVERE, "Invalid Format: {0}", ex);
        }
        fc.responseComplete(); // Important! Otherwise JSF will attempt to render the response which obviously will fail since it's already written with a file and closed.

    }
    
    public List<SelectItem> getGroup_values() {
        return group_values;
    }
    
    public boolean isNr_crt() {
        return nr_crt;
    }
    
    public void setNr_crt(boolean nr_crt) {
        this.nr_crt = nr_crt;
    }
    
    public boolean isTotal_row() {
        return total_row;
    }
    
    public void setTotal_row(boolean total_row) {
        this.total_row = total_row;
    }
    
    public boolean isTotal_col() {
        return total_col;
    }
    
    public void setTotal_col(boolean total_col) {
        this.total_col = total_col;
    }
    
    public String getGroup_row1() {
        return group_row1;
    }
    
    public void setGroup_row1(String group_row1) {
        this.group_row1 = group_row1;
    }
    
    public String getGroup_row2() {
        return group_row2;
    }
    
    public void setGroup_row2(String group_row2) {
        this.group_row2 = group_row2;
    }
    
    public String getGroup_row3() {
        return group_row3;
    }
    
    public void setGroup_row3(String group_row3) {
        this.group_row3 = group_row3;
    }
    
    public String getGroup_col1() {
        return group_col1;
    }
    
    public void setGroup_col1(String group_col1) {
        this.group_col1 = group_col1;
    }
    
    public List<SelectItem> getCol_values() {
        return col_values;
    }
}
