/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.bdje.beans;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.primefaces.event.FileUploadEvent;
import ro.deimios.bdje.datatype.ImportException;
import ro.deimios.bdje.entity.Alternativa;
import ro.deimios.bdje.entity.Clasa;
import ro.deimios.bdje.entity.Elev;
import ro.deimios.bdje.entity.Forma;
import ro.deimios.bdje.entity.Mediul;
import ro.deimios.bdje.entity.Sectia;
import ro.deimios.bdje.entity.Unitate;
import ro.deimios.bdje.facade.AlternativaFacadeLocal;
import ro.deimios.bdje.facade.ClasaFacadeLocal;
import ro.deimios.bdje.facade.ElevFacadeLocal;
import ro.deimios.bdje.facade.FormaFacadeLocal;
import ro.deimios.bdje.facade.MediulFacadeLocal;
import ro.deimios.bdje.facade.SectiaFacadeLocal;
import ro.deimios.bdje.facade.UnitateFacadeLocal;
import ro.deimios.bdje.facade.helper.BeanHelper;
import ro.deimios.bdje.facade.helper.CNPHelper;

/**
 *
 * @author deimios
 */
@ManagedBean
@ViewScoped
public class ImportBean implements Serializable {

    @EJB
    private SectiaFacadeLocal sectiaFacade;
    @EJB
    private MediulFacadeLocal mediulFacade;
    @EJB
    private FormaFacadeLocal formaFacade;
    @EJB
    private ClasaFacadeLocal clasaFacade;
    @EJB
    private AlternativaFacadeLocal alternativaFacade;
    @EJB
    private ElevFacadeLocal elevFacade;
    @EJB
    private UnitateFacadeLocal unitateFacade;
    private String importLog;
    private static final Logger LOG = Logger.getLogger(ImportBean.class.getName());

    /**
     * Creates a new instance of ImportBean
     */
    public ImportBean() {
    }

    public void handleFileUpload(FileUploadEvent event) throws FileNotFoundException, IOException {
        try {
            importLog = "";
            InputStream is = event.getFile().getInputstream();
            Workbook workbook = WorkbookFactory.create(is);
            Sheet sheet = workbook.getSheetAt(0);
            int rows = sheet.getLastRowNum() + 1;
            //LOG.log(Level.INFO, "File uploaded. Rows: {0}", rows);
            UserBean userBean = (UserBean) BeanHelper.getBean("userBean");
            int userSirues = userBean.user.getUnitate().getSirues();

            int imported = 0, edited = 0;
            boolean isEdit = false;

            //skip header then loop rows
            for (int i = 1; i < rows; i++) {
                try {
                    Row row = sheet.getRow(i);

                    long lCNP;
                    String sCNP;

                    try {
                        lCNP = Math.round(row.getCell(0).getNumericCellValue());
                        sCNP = String.valueOf(lCNP);
                    } catch (IllegalStateException isex) {
                        sCNP = row.getCell(0).getStringCellValue();
                        lCNP = Long.parseLong(sCNP);
                    }

                    String errors = CNPHelper.validate(sCNP);

                    if (errors != null) {
                        throw new ImportException("Rândul " + (i + 1) + ": " + errors);
                    }
                    Elev dbElev = elevFacade.find(lCNP);
                    if (dbElev == null) {
                        //not found, insert
                        dbElev = new Elev(lCNP);
                        isEdit = false;
                    } else {
                        isEdit = true;
                        //found, check unitate
                        if ((dbElev.getUnitate().getSuperior().getSirues() != userSirues) && !userBean.isISJ()) {
                            throw new ImportException("Rândul " + (i + 1) + ": elevul " + dbElev.getNume() + " " + dbElev.getPrenume() + " este deja înscris la " + dbElev.getUnitate().getSuperior().getDenumire());
                        }
                    }

                    String nume, ini, prenume;

                    Unitate unitate;

                    //nume
                    if (row.getCell(1) == null) {
                        throw new ImportException("Rândul " + (i + 1) + ": Nume necompletat");
                    } else {
                        nume = row.getCell(1).getStringCellValue();
                        if (nume.length() == 0) {
                            throw new ImportException("Rândul " + (i + 1) + ": Nume necompletat");
                        }
                    }


                    //ini
                    if (row.getCell(2) == null) {
                        throw new ImportException("Rândul " + (i + 1) + ": Inițiala necompletată");
                    } else {
                        ini = row.getCell(2).getStringCellValue();
                        if (ini.length() != 1) {
                            throw new ImportException("Rândul " + (i + 1) + ": Inițiala necompletată sau prea lungă");
                        }
                    }



                    if (row.getCell(3) == null) {
                        throw new ImportException("Rândul " + (i + 1) + ": Prenume necompletat");
                    } else {
                        prenume = row.getCell(3).getStringCellValue();
                        if (prenume.length() == 0) {
                            throw new ImportException("Rândul " + (i + 1) + ": Prenume necompletat");
                        }
                    }

                    if (row.getCell(4) == null) {
                        throw new ImportException("Rândul " + (i + 1) + ": Unitate necompletată");
                    }

                    unitate = unitateFacade.findByDenumire(row.getCell(4).getStringCellValue());

                    if (unitate == null) {
                        throw new ImportException("Rândul " + (i + 1) + ": unitatea " + row.getCell(4).getStringCellValue() + " nu există");
                    }

                    if ((unitate.getSuperior().getSirues() != userSirues) && !userBean.isISJ()) {
                        throw new ImportException("Rândul " + (i + 1) + ": unitatea " + unitate.getDenumire() + " nu aparține PJ-ului Dvs.");
                    }

                    if (row.getCell(5) == null) {
                        throw new ImportException("Rândul " + (i + 1) + ": Grupa/Clasa necompletată");
                    }
                    Clasa dbClasa = clasaFacade.findByNume(row.getCell(5).getStringCellValue());
                    if (dbClasa == null) {
                        throw new ImportException("Rândul " + (i + 1) + ": Grupa/Clasa invalidă");
                    }

                    String numeClasa = "";

                    if (row.getCell(6) != null) {
                        numeClasa = row.getCell(6).getStringCellValue();
                    }

                    Alternativa dbAlternativa;
                    if (row.getCell(7) == null) {
                        dbAlternativa = alternativaFacade.find(1); //default tradițională
                    } else {
                        dbAlternativa = alternativaFacade.findByNume(row.getCell(7).getStringCellValue());
                        if (dbAlternativa == null) {
                            dbAlternativa = alternativaFacade.find(1); //default tradițională
                        }
                    }

                    Sectia dbSectia;
                    if (row.getCell(8) == null) {
                        throw new ImportException("Rândul " + (i + 1) + ": Secția necompletată");
                    }
                    dbSectia = sectiaFacade.findByNume(row.getCell(8).getStringCellValue());
                    if (dbSectia == null) {
                        throw new ImportException("Rândul " + (i + 1) + ": Secția invalidă");
                    }

                    Forma dbForma;
                    if (row.getCell(9) == null) {
                        //empty
                        if (dbClasa.getId() < 5) {
                            //gradinita
                            dbForma = formaFacade.find(1); //normal
                        } else {
                            //scoala
                            dbForma = formaFacade.find(3); //Zi
                        }
                    } else {
                        //find data
                        dbForma = formaFacade.findByNume(row.getCell(9).getStringCellValue());
                        if (dbForma == null) {
                            if (dbClasa.getId() < 5) {
                                //gradinita
                                dbForma = formaFacade.find(1); //normal
                            } else {
                                //scoala
                                dbForma = formaFacade.find(3); //Zi
                            }
                        }
                    }

                    Mediul dbMediul;
                    if (row.getCell(10) == null) {
                        throw new ImportException("Rândul " + (i + 1) + ": Mediul necompletat");
                    }
                    dbMediul = mediulFacade.findByNume(row.getCell(10).getStringCellValue());
                    if (dbMediul == null) {
                        throw new ImportException("Rândul " + (i + 1) + ": Mediul invalid");
                    }

                    //set data if all is ok
                    dbElev.setNume(nume);
                    dbElev.setIni(ini);
                    dbElev.setPrenume(prenume);
                    dbElev.setUnitate(unitate);
                    dbElev.setClasa(dbClasa);
                    dbElev.setNumeClasa(numeClasa);
                    dbElev.setAlternativa(dbAlternativa);
                    dbElev.setSectia(dbSectia);
                    dbElev.setForma(dbForma);
                    dbElev.setMediul(dbMediul);

                    if (isEdit) {
                        elevFacade.edit(dbElev);
                        edited++;
                    } else {
                        elevFacade.create(dbElev);
                        imported++;
                    }

                } catch (ImportException iex) {
                    importLog = importLog + "\r\n" + iex.getMessage();
                    //iex.printStackTrace();
                } catch (Exception ex) {
                    importLog = importLog + "\r\n" + "Rândul " + (i + 1) + ": Eroare";
                    //ex.printStackTrace();
                }
            }
            importLog = importLog + "\r\n" + "Importul s-a terminat. S-au importat " + imported + " elevi noi și s-au editat datele la " + edited + " elevi";
            //LOG.info(importLog);
            is.close();
        } catch (InvalidFormatException ex) {
            //ex.printStackTrace();
        }

    }

    public String getImportLog() {
        return importLog;
    }
}
