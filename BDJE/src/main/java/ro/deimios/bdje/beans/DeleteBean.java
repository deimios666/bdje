package ro.deimios.bdje.beans;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import ro.deimios.bdje.entity.Elev;
import ro.deimios.bdje.facade.ElevFacadeLocal;
import ro.deimios.bdje.facade.helper.BeanHelper;

/**
 *
 * @author deimios
 */
@ManagedBean
@RequestScoped
public class DeleteBean implements Serializable {

    @EJB
    private ElevFacadeLocal elevFacade;
    private String cnp;
    private String deleteLog;

    /**
     * Creates a new instance of DeleteBean
     */
    public DeleteBean() {
    }

    public String getCnp() {
        return cnp;
    }

    public void setCnp(String cnp) {
        this.cnp = cnp;
    }

    public String getDeleteLog() {
        return deleteLog;
    }

    public void deleteCnp() {
        deleteLog = "";
        long lCNP = Long.parseLong(this.cnp);
        Elev elev = elevFacade.find(lCNP);
        if (elev == null) {
            deleteLog = "CNP inexistent sau elevul nu aparține unității";
        } else {
            UserBean userBean = (UserBean) BeanHelper.getBean("userBean");
            int userSirues = userBean.user.getUnitate().getSirues();
            if ((elev.getUnitate().getSuperior().getSirues() != userSirues) && !userBean.isISJ()) {
                deleteLog = "CNP inexistent sau elevul nu aparține unității";
            } else {
                elevFacade.remove(elev);
                deleteLog = "Elevul a fost șters";
            }
        }
    }
}
