package ro.deimios.bdje.datatype;

/**
 *
 * @author deimios
 */
public class ImportException extends Exception {

    public ImportException(String message) {
        super(message);
    }
}
