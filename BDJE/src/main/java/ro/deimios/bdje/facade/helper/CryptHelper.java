package ro.deimios.bdje.facade.helper;

import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author Deimios
 */
public class CryptHelper {

    public static String sha512(String value) {
        String pHash = "";
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            byte[] byteData = md.digest(value.getBytes());
            StringBuilder hexString = new StringBuilder();
            for (int i = 0; i < byteData.length; i++) {
                String hex = Integer.toHexString(0xff & byteData[i]);
                if (hex.length() == 1) {
                    hexString.append('0');
                }
                hexString.append(hex);
            }
            pHash = hexString.toString();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(CryptHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pHash;
    }

    public static String sha512(File value) throws FileNotFoundException, IOException {
        String pHash = "";
        try {
            InputStream fis = new FileInputStream(value);
            byte[] buffer = new byte[1024];
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            int numRead;
            do {
                numRead = fis.read(buffer);
                if (numRead > 0) {
                    md.update(buffer, 0, numRead);
                }
            } while (numRead != -1);

            fis.close();
            byte[] byteData = md.digest();
            StringBuilder hexString = new StringBuilder();
            for (int i = 0; i < byteData.length; i++) {
                String hex = Integer.toHexString(0xff & byteData[i]);
                if (hex.length() == 1) {
                    hexString.append('0');
                }
                hexString.append(hex);
            }
            pHash = hexString.toString();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(CryptHelper.class.getName()).log(Level.SEVERE, null, ex);
        }

        return pHash;
    }

    public static String sha512(UploadedFile value) throws FileNotFoundException, IOException {
        String pHash = "";
        try {
            InputStream fis = value.getInputstream();
            byte[] buffer = new byte[1024];
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            int numRead;
            do {
                numRead = fis.read(buffer);
                if (numRead > 0) {
                    md.update(buffer, 0, numRead);
                }
            } while (numRead != -1);

            fis.close();
            byte[] byteData = md.digest();
            StringBuilder hexString = new StringBuilder();
            for (int i = 0; i < byteData.length; i++) {
                String hex = Integer.toHexString(0xff & byteData[i]);
                if (hex.length() == 1) {
                    hexString.append('0');
                }
                hexString.append(hex);
            }
            pHash = hexString.toString();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(CryptHelper.class.getName()).log(Level.SEVERE, null, ex);
        }

        return pHash;
    }
}
