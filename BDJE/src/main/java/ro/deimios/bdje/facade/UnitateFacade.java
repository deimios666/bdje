/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.bdje.facade;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ro.deimios.bdje.entity.Unitate;

/**
 *
 * @author deimios
 */
@Stateless
public class UnitateFacade extends AbstractFacade<Unitate> implements UnitateFacadeLocal {

    @PersistenceContext(unitName = "ro.deimios_BDJE_war_1.0PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UnitateFacade() {
        super(Unitate.class);
    }

    @Override
    public List<Unitate> findBySuperior(int sirues) {
        Query query = em.createNamedQuery("Unitate.findBySuperior");
        query.setParameter("sirues", sirues);
        return query.getResultList();
    }

    @Override
    public List<Unitate> findAllOrdered() {
        Query query = em.createNamedQuery("Unitate.findAll");
        return query.getResultList();
    }

    @Override
    public Unitate findByDenumire(String denumire) {
        Query query = em.createNamedQuery("Unitate.findByDenumire");
        query.setParameter("denumire", denumire);
        try {
            return (Unitate) query.getSingleResult();
        } catch (NoResultException nrex) {
            return null;
        }
    }
}
