package ro.deimios.bdje.facade.helper;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 *
 * @author deimios
 */
public class CNPHelper {

    public static String validate(String cnp) {
        String errors = "";

        //check length
        if (cnp.length() != 13) {
            errors += "Lungime invalidă!\n";
            return errors;
        }

        //check sex/century
        String sexCentury = cnp.substring(0, 1);
        int sc = Integer.valueOf(sexCentury);
        String century = "";

        if (sc == 0) {
            errors += "Sexul nu poate fi 0\n";
            return errors;
        } else if (sc < 3) {
            century = "19";
        } else if (sc < 5) {
            century = "18";
        } else if (sc < 7) {
            century = "20";
        }

        if (sc < 7) {
            //we only check the rest of the data for residents
            //check birthdate
            try {
                DateFormat df = new SimpleDateFormat("yyyyMMdd");
                df.setLenient(false);
                df.parse(century + cnp.substring(1, 7));

            } catch (ParseException e) {
                errors += "Data nașterii invalidă\n";
                return errors;
            }
            //check judet
            /*int judet = Integer.valueOf(cnp.substring(7, 9));
            if (!(judet > 0 && (judet < 47 || judet == 51 || judet == 52))) {
                errors += "Cod județ invalid\n";
                return errors;
            }*/
        }

        //check checksum
        int[] checkSum = {2, 7, 9, 1, 4, 6, 3, 5, 8, 2, 7, 9};
        int sum = 0;
        for (int i = 0; i < 12; i++) {
            sum = sum + (Integer.valueOf(cnp.substring(i, i + 1)) * checkSum[i]);
        }
        sum = sum % 11;
        if (sum == 10) {
            sum = 1;
        }
        if (Integer.valueOf(cnp.substring(12, 13)) != sum) {
            errors += "Erorare cod validare CNP";
            return errors;
        }
        return null;
    }

    public static int getCheckSum(String cnp) {

        //check checksum
        int[] checkSum = {2, 7, 9, 1, 4, 6, 3, 5, 8, 2, 7, 9};
        int sum = 0;
        for (int i = 0; i < 12; i++) {
            sum = sum + (Integer.valueOf(cnp.substring(i, i + 1)) * checkSum[i]);
        }
        sum = sum % 11;
        if (sum == 10) {
            sum = 1;
        }
        return sum;
    }
}
