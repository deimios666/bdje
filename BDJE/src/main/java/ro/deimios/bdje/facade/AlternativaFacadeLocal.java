/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.bdje.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.bdje.entity.Alternativa;

/**
 *
 * @author deimios
 */
@Local
public interface AlternativaFacadeLocal {

    void create(Alternativa alternativa);

    void edit(Alternativa alternativa);

    void remove(Alternativa alternativa);

    Alternativa find(Object id);

    List<Alternativa> findAll();

    List<Alternativa> findRange(int[] range);

    int count();

    public Alternativa findByNume(String nume);
    
}
