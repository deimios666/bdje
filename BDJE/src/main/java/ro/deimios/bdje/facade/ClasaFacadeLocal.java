/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.bdje.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.bdje.entity.Clasa;

/**
 *
 * @author deimios
 */
@Local
public interface ClasaFacadeLocal {

    void create(Clasa clasa);

    void edit(Clasa clasa);

    void remove(Clasa clasa);

    Clasa find(Object id);

    List<Clasa> findAll();

    List<Clasa> findRange(int[] range);

    int count();

    public Clasa findByNume(String nume);
    
}
