/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.bdje.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ro.deimios.bdje.entity.Forma;

/**
 *
 * @author deimios
 */
@Stateless
public class FormaFacade extends AbstractFacade<Forma> implements FormaFacadeLocal {

    @PersistenceContext(unitName = "ro.deimios_BDJE_war_1.0PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FormaFacade() {
        super(Forma.class);
    }

    @Override
    public Forma findByNume(String nume) {
        Query query = em.createNamedQuery("Forma.findByNume");
        query.setParameter("nume", nume);

        try {
            return (Forma) query.getSingleResult();
        } catch (Exception ex) {
            return null;
        }
    }
}
