/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.bdje.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.bdje.entity.Forma;

/**
 *
 * @author deimios
 */
@Local
public interface FormaFacadeLocal {

    void create(Forma forma);

    void edit(Forma forma);

    void remove(Forma forma);

    Forma find(Object id);

    List<Forma> findAll();

    List<Forma> findRange(int[] range);

    int count();

    public Forma findByNume(String nume);
    
}
