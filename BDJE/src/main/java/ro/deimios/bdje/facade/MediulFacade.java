/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.bdje.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ro.deimios.bdje.entity.Mediul;

/**
 *
 * @author deimios
 */
@Stateless
public class MediulFacade extends AbstractFacade<Mediul> implements MediulFacadeLocal {
    @PersistenceContext(unitName = "ro.deimios_BDJE_war_1.0PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MediulFacade() {
        super(Mediul.class);
    }
    
    @Override
    public Mediul findByNume(String nume) {
        Query query = em.createNamedQuery("Mediul.findByNume");
        query.setParameter("nume", nume);

        try {
            return (Mediul) query.getSingleResult();
        } catch (Exception ex) {
            return null;
        }
    }
    
}
