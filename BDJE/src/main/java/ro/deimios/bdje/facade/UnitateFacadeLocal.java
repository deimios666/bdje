/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.bdje.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.bdje.entity.Unitate;

/**
 *
 * @author deimios
 */
@Local
public interface UnitateFacadeLocal {

    void create(Unitate unitate);

    void edit(Unitate unitate);

    void remove(Unitate unitate);

    Unitate find(Object id);

    List<Unitate> findAll();

    List<Unitate> findRange(int[] range);

    int count();

    public List<Unitate> findBySuperior(int sirues);

    public List<Unitate> findAllOrdered();

    public Unitate findByDenumire(String denumire);
    
}
