package ro.deimios.bdje.facade.helper;

import javax.faces.application.Application;
import javax.faces.context.FacesContext;
import javax.faces.el.ValueBinding;

/**
 *
 * @author Deimios
 */
public class BeanHelper {

    public static Object getBean(String expr) {
        FacesContext context = FacesContext.getCurrentInstance();
        Application app = context.getApplication();
        ValueBinding binding = app.createValueBinding("#{" + expr + "}");
        Object value = binding.getValue(context);
        return value;
    }
}
