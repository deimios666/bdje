/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.bdje.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ro.deimios.bdje.entity.Clasa;

/**
 *
 * @author deimios
 */
@Stateless
public class ClasaFacade extends AbstractFacade<Clasa> implements ClasaFacadeLocal {

    @PersistenceContext(unitName = "ro.deimios_BDJE_war_1.0PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ClasaFacade() {
        super(Clasa.class);
    }

    @Override
    public Clasa findByNume(String nume) {
        Query query = em.createNamedQuery("Clasa.findByNume");
        query.setParameter("nume", nume);

        try {
            return (Clasa) query.getSingleResult();
        } catch (Exception ex) {
            return null;
        }
    }
}
