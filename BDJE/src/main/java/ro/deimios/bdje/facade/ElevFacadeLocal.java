/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.bdje.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.bdje.entity.Elev;

/**
 *
 * @author deimios
 */
@Local
public interface ElevFacadeLocal {

    void create(Elev elev);

    void edit(Elev elev);

    void remove(Elev elev);

    Elev find(Object id);

    List<Elev> findAll();

    List<Elev> findRange(int[] range);

    int count();

    public List<Elev> findByPJ(int sirues);

    public List<Object[]> getReport(String colgroup, List<String> rowgroups, int sirues);

    public List<Object[]> getReport(String colgroup, List<String> rowgroups);
    
}
