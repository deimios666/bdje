/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.bdje.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.bdje.entity.User;

/**
 *
 * @author deimios
 */
@Local
public interface UserFacadeLocal {

    void create(User user);

    void edit(User user);

    void remove(User user);

    User find(Object id);

    List<User> findAll();

    List<User> findRange(int[] range);

    int count();

    public User findByLogin(String username, String password);
    
}
