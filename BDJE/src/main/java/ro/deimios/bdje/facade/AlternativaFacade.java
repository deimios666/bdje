/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.bdje.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ro.deimios.bdje.entity.Alternativa;

/**
 *
 * @author deimios
 */
@Stateless
public class AlternativaFacade extends AbstractFacade<Alternativa> implements AlternativaFacadeLocal {
    @PersistenceContext(unitName = "ro.deimios_BDJE_war_1.0PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AlternativaFacade() {
        super(Alternativa.class);
    }
    
    @Override
    public Alternativa findByNume(String nume) {
        Query query = em.createNamedQuery("Alternativa.findByNume");
        query.setParameter("nume", nume);

        try {
            return (Alternativa) query.getSingleResult();
        } catch (Exception ex) {
            return null;
        }
    }
    
}
