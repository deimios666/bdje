/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.bdje.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.bdje.entity.Sectia;

/**
 *
 * @author deimios
 */
@Local
public interface SectiaFacadeLocal {

    void create(Sectia sectia);

    void edit(Sectia sectia);

    void remove(Sectia sectia);

    Sectia find(Object id);

    List<Sectia> findAll();

    List<Sectia> findRange(int[] range);

    int count();

    public Sectia findByNume(String nume);
    
}
