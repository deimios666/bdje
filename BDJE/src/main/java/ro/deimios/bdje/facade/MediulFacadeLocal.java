/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.bdje.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.bdje.entity.Mediul;

/**
 *
 * @author deimios
 */
@Local
public interface MediulFacadeLocal {

    void create(Mediul mediul);

    void edit(Mediul mediul);

    void remove(Mediul mediul);

    Mediul find(Object id);

    List<Mediul> findAll();

    List<Mediul> findRange(int[] range);

    int count();

    public Mediul findByNume(String nume);
    
}
