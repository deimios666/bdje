/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.bdje.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ro.deimios.bdje.entity.Sectia;

/**
 *
 * @author deimios
 */
@Stateless
public class SectiaFacade extends AbstractFacade<Sectia> implements SectiaFacadeLocal {
    @PersistenceContext(unitName = "ro.deimios_BDJE_war_1.0PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SectiaFacade() {
        super(Sectia.class);
    }
    
    @Override
    public Sectia findByNume(String nume) {
        Query query = em.createNamedQuery("Sectia.findByNume");
        query.setParameter("nume", nume);

        try {
            return (Sectia) query.getSingleResult();
        } catch (Exception ex) {
            return null;
        }
    }
    
}
