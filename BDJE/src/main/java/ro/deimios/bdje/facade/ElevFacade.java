package ro.deimios.bdje.facade;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ro.deimios.bdje.entity.Elev;

/**
 *
 * @author deimios
 */
@Stateless
public class ElevFacade extends AbstractFacade<Elev> implements ElevFacadeLocal {

    private static final Logger LOG = Logger.getLogger(ElevFacade.class.getName());
    @PersistenceContext(unitName = "ro.deimios_BDJE_war_1.0PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ElevFacade() {
        super(Elev.class);
    }

    @Override
    public List<Elev> findByPJ(int sirues) {
        Query query = em.createNamedQuery("Elev.findByPJ");
        query.setParameter("sirues", sirues);
        return query.getResultList();
    }

    @Override
    public List<Object[]> getReport(String colgroup, List<String> rowgroups, int sirues) {
        List<String> colNames = new ArrayList<>();
        List<Object[]> retVal = new ArrayList<>();
        String query_part_1 = "select ";
        String query_part_2;
        if (sirues > 0) {
            query_part_2 = "from Elev e where e.unitate.superior.sirues = :sirues group by  ";
        } else {
            query_part_2 = "from Elev e group by  ";
        }


        if (rowgroups.isEmpty()) {
            //no row grouping
            query_part_2 += "e.unitate.superior.sirues,";
        } else {
            //row grouping
            for (int i = 0; i < rowgroups.size(); i++) {
                String rowgrouping = rowgroups.get(i);
                query_part_1 += "e." + rowgrouping + ",";
                query_part_2 += "e." + rowgrouping + ",";
                colNames.add(rowgrouping);
            }
        }

        if (colgroup == null || colgroup.isEmpty()) {
            //no column grouping
        } else {
            //column grouping
            //get column values
            Query colQuery = em.createQuery("select x.id, x.nume from " + colgroup + " x");
            List<Object[]> results = colQuery.getResultList();
            for (int i = 0; i < results.size(); i++) {
                Object[] curResult = results.get(i);
                int colId = (int) curResult[0];
                String colName = (String) curResult[1];
                String subColGroup = colgroup.toLowerCase();
                colNames.add(colName);
                String oneLine = "sum(case e." + subColGroup + ".id when " + colId + " then 1 else 0 end) as c" + i + ",";
                query_part_1 += oneLine;
            }

        }
        //build query

        //add count
        query_part_1 += "count(e) AS Total ";
        colNames.add("Total");
        //strip last comma
        //query_part_1 = query_part_1.substring(0, query_part_1.length() - 1);
        query_part_2 = query_part_2.substring(0, query_part_2.length() - 1);

        String queryString = query_part_1 + " " + query_part_2;

        Query query = em.createQuery(queryString);
        if (sirues > 0) {
            query.setParameter("sirues", sirues);
        }


        retVal.add(colNames.toArray());
        retVal.addAll(query.getResultList());
        return retVal;
    }

    @Override
    public List<Object[]> getReport(String colgroup, List<String> rowgroups) {
        return getReport(colgroup, rowgroups, 0);
    }
}
